import React, {component, Component} from 'react';
import PropTypes from 'prop-types';
import ForecastItem from './ForecastItem';
import transformForecast from './../services/transformForecast';
import LinearProgress from 'material-ui/LinearProgress';
import './styles.css'

const url = "http://api.openweathermap.org/data/2.5/forecast";
const api_key = "f99bbd9e4959b513e9bd0d7f7356b38d";
class ForecastExtended extends Component{
    constructor() {
        super();
        this.state = {forecastData: null}
    }

    componentDidMount() {
        //fetch or axios
        const url_forcast = `${url}?q=${this.props.city}&appid=${api_key}&units=metric`;
        fetch(url_forcast).then(
            data => (data.json())
        ).then(
            weather_data => {
                console.log(weather_data);
                const forecastData = transformForecast(weather_data);
                console.log(forecastData); 
                this.setState({forecastData});
            }
        )
    }
    
    renderForecastItemDays(forecastData) {
        return forecastData.map(forecast => (
            <ForecastItem
                  key={`${forecast.weekDay}${forecast.hour}`}
                  weekDay={forecast.weekDay} 
                  hour={forecast.hour} 
                  data={forecast.data}>
            </ForecastItem>
        ));
    }
    render(){
        const {city} = this.props;
        const {forecastData} = this.state;
        return (
            <div>
                <h2 className='forecast-title'> Pronóstico Extendido para {city} </h2>
                {forecastData ?
                    this.renderForecastItemDays(forecastData) :
                    <LinearProgress size={50} thickness={6}/>
                }
            </div>
            );
    }
}

ForecastExtended.protoTypes = {
    city: PropTypes.string.isRequired,
}
export default ForecastExtended;