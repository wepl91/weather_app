import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

const Location = ({city}) => { //Destructuring desde el parametro
    //Destructuring. En vez de const city = props.city 
    //ya que la variable y la prop tienen mismo nombre 
    //const {city} = props; 
    return (
        <div className='LocationCont'>
            <h1>
                {city}
            </h1>
        </div>);

};

Location.propTypes = {
    city: PropTypes.string.isRequired,
}    
export default Location;