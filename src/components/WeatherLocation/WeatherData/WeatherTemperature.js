import React from 'react';
import WeatherIcons from 'react-weathericons';
import {SNOW, WINDY, RAIN, SUN, CLOUDY, CLOUD, THUNDER, DRIZZLE} from './../../../constants/Weathers.js'
import PropTypes from 'prop-types';
import './styles.css';

const stateToIconName = (weatherState) => {
    switch (weatherState) {
        case SNOW:
            return "snow";
        case WINDY:
            return "windy";
        case RAIN:
            return "rain";
        case SUN:
            return "sun";
        case CLOUDY:
            return "cloudy";
        case CLOUD:
            return "cloud";
        case THUNDER:
            return "thunder";
        case DRIZZLE:
            return "drizzle";
        default:
            return "day-sunny";
    }
};

const getWeatherIcon = (weatherState) => {
   return (<WeatherIcons className='wicon'name={stateToIconName(weatherState)} size="4x" />)
}

const WeatherTemperature = ({temperature, weatherState}) => (
    <div className='weatherTemperatureCont'>
        {getWeatherIcon(weatherState)}
        <span className='temperature'>{`${temperature}`}</span>
        <span className='temperatu  reType'>C°</span>
    </div>
);

//Validaciones del componente antes de su exportacion
WeatherTemperature.propTypes = {
    temperature: PropTypes.number.isRequired,
    weatherState: PropTypes.string,
}
export default WeatherTemperature;